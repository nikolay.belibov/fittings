const getScrollXY = {
    scrOfX: () => {
        let scrOfX = 0;
        if ( typeof ( window.pageYOffset ) == 'number' ) {
            // Netscape compliant
            scrOfX = window.pageXOffset;
        } else if ( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
            // DOM compliant
            scrOfX = document.body.scrollLeft;
        } else if ( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
            // IE6 standards compliant mode
            scrOfX = document.documentElement.scrollLeft;
        }
        // console.log(scrOfX);
        return scrOfX;
    },
    scrOfY: () => {
        let scrOfY = 0;
        if ( typeof ( window.pageYOffset ) == 'number' ) {
            // Netscape compliant
            scrOfY = window.pageYOffset;
        } else if ( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
            // DOM compliant
            scrOfY = document.body.scrollTop;
        } else if ( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
            // IE6 standards compliant mode
            scrOfY = document.documentElement.scrollTop;
        }
        // console.log(scrOfY);
        return scrOfY;
    }
};
const getCoords = (elem) => {
    const box = elem.getBoundingClientRect();
    const body = document.body;
    const docEl = document.documentElement;
    const scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    const scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;
    const clientTop = docEl.clientTop || body.clientTop || 0;
    const clientLeft = docEl.clientLeft || body.clientLeft || 0;
    const top = box.top + scrollTop - clientTop;
    const left = box.left + scrollLeft - clientLeft;
    return {
        top: top,
        left: left
    };
};
const sectionTopPosition = (selector) => {
    if (document.querySelector(selector)) {
        return document.querySelector(selector).getBoundingClientRect().top + window.pageYOffset;
    }
};
const sectionBottomPosition = (selector) => {
    if (document.querySelector(selector)) {
        return document.querySelector(selector).getBoundingClientRect().bottom + window.pageYOffset;
    }
};

let wnd = $(window);
let opacityControl = $('header');
let header = document.querySelector('.header');
let navList = document.querySelector('.nav-list');
let linkList = navList.querySelectorAll('a');
let brand = document.querySelector('.brand');

let fillHeader = () => {
    let top = wnd.scrollTop();
    let opacity = top > 500 ? 1 : top * 2 / 1000;
    opacityControl.css('background', 'rgba(237,241,247,' + opacity + ')');
};

let navigateTo = (list, evt) => {
    list.addEventListener(evt, (e) => {
        e.preventDefault();
        e.stopPropagation();
        let _target = e.target;
        let link = _target.closest('a');
        let hashId = link.getAttribute('href').slice(1);
        if (!link) {
            return;
        } else {
            let coord = getCoords(document.getElementById(hashId));
            let callback = () => {
                $('.nav-list a').removeClass('is-active');
                _target.classList.add('is-active');
            };
            $('html, body').animate({
                scrollTop: coord.top - header.clientHeight
            }, 1000, callback);
        }
    });
};

let brandTo = (selector, evt) => {
    selector.addEventListener(evt, (e) => {
        e.preventDefault();
        e.stopPropagation();
        let _target = e.target;
        let link = _target.closest('a');
        let hashId = link.getAttribute('href').slice(1);
        if (!link) {
            return;
        } else {
            let coord = getCoords(document.getElementById(hashId));
            $('html, body').animate({
                scrollTop: coord.top - header.clientHeight
            }, 1000);
        }
    });
};

export default {
    init() {
        fillHeader();
        navigateTo(navList, 'click');
        brandTo(brand, 'click');
    },
    scroll() {
        wnd.scroll(function () {
            fillHeader();
        });
    }
};
