'use strict';

import polyfills from './libraries/polyfills';
import funcHeader from 'components/header/header';
import funcYmaps from 'components/map/map';
import funcRedstar from 'components/redstar/redstar';
import WOW from 'wow.js';


$(() => {
    polyfills.init();
    // ================ Здесь инициализируем модули =====================
    funcHeader.init();
    funcRedstar.init();
    // new WOW().init();
});

funcHeader.scroll();
